
import argparse
import os
import sys

import tensorflow as tf
import matplotlib.pyplot as plt 
import numpy as np

from tensorpack import *
from tensorpack.callbacks import graph
from tensorpack.dataflow import dataset
#from tensorpack.predict.base import CreateAdvAttacks
from tensorpack.tfutils.sessinit import SmartInit
from tensorpack.tfutils.summary import add_moving_summary, add_param_summary
from enum import Enum

from tensorpack.input_source import PlaceholderInput
from tensorpack.tfutils.common import get_tensors_by_names, get_op_tensor_name
from tensorpack.tfutils.tower import PredictTowerContext
from tensorpack.tfutils.varreplace import remap_variables
#if i call this, this will call the updated version of the file in my PC but without it tensorpack import * will call the defaule version somehow
#from tensorpack.predict.base import OfflinePredictor

from art.attacks.evasion import FastGradientMethod
from art.estimators.classification import TensorFlowClassifier

#Get custom Mnist data extraction (default tensorflow has an addition dimention that can cause problems during inference)
from DataSets.mnist import GetMnist
from Inference.infer_model import infer_model
from CreateAttacks.supported_attacks import SupportedAlgorithms
from CreateAttacks.create_adversarial_samples import get_adversarial_samples
import save_restore_images
import visualize_data

from dorefa import get_dorefa


BITW = 2 # Bitwidth of weight
BITA = 2 # Bitwidth of activations
BITG = 32 #Bitwidth of gradients
Mnist_Data_Dir = r"C:\Users\sab\Downloads\AI Testing\_Tools\DataSets\MNIST\Data"

"""
    BIAS QUANTIZATION: 
    Biases are not quantized by DoReFa because according to authors they contribute less in network size and performance so 
    quantizing them is not helpful for performance or for size reduction. 
    Ref: pending (is in github issues responses)
    GRADIENT QUANTIZATION:
    DoReFaNet supports gradient quantization. In fact, one of its achievement is being able to quantize gradients to 8 bits and lower
    and still have comparable accuracy (atleast for 8 bits)
    The main advantage of gradient quantization is that it allows bitwise operations during backpropagation as well where kernels
    can be operated with gradients without float point multiplication but instead use bitwise operation. This significantly boosts
    training speeds further, transfering gradients from one network to another during parallel training is faster when bitwidth is smaller.
    ref: 
        https://arxiv.org/pdf/1606.06160.pdf, section 1 and 2.1
        https://arxiv.org/pdf/1808.04752.pdf, section 4.3
    Gradient quantization is not necessary for our research since we do not need to take into account performance boost during training.    
"""

"""
    Define model using the ModelDesc parent. To train or to inference a network, you would require 
    1. The dataset (to train or infer from)
    2. The model definition
    3. The input placeholder which is used to later feed data
    4. Optimizer function (required by trainer only)

    Three of these four (2,3,4) are supplied by the ModelDesc. Thus this class the core of tensorpack understanding and a
    model should be restored (especially quantized) using the ModelDesc description in order to make sure the graph is made correctly
    ref: 
        1. https://tensorpack.readthedocs.io/en/latest/tutorial/inference.html#step-1-build-the-model-graph
        2. https://tensorpack.readthedocs.io/tutorial/training-interface.html#with-modeldesc-and-trainconfig
"""

class Model(ModelDesc):

    # Provide the input signature
    # Here it should be noted that the LeNet architecture that we are impmlementing requires images to be in 32x32 format
    # By default the image size is 28x28 for MNIST, so need to resize images before feeding to the network
    def inputs(self):
        return [tf.TensorSpec((None, 28, 28), tf.float32, 'input'),
                tf.TensorSpec((None,), tf.int32, 'label')]

    #define the model
    def build_graph(self, input, label):
        """
            The default dataset for MNIST only has 3 dim (Batch, image_height, Image_width). In tf, one addition dimension
            for channel is required so add one additional channel at axis =3
        """
        input = tf.expand_dims(input, 3)

        # get quantization functions from dorefa
        fw, fa, fg = get_dorefa(BITW, BITA, BITG)


        def quantize_weight(v):
            name = v.op.name
            # don't binarize first and last layer
            if not name.endswith('W') or 'conv0' in name or 'linear' in name:
                return v
            else:
                logger.info("Quantizing weights to {} bits {}".format(BITW, v.op.name)) #v.name = conv1/W:0; v.op.name = conv1/W
                return fw(v)

        def nonlin(x):
            if BITA == 32:
                return tf.nn.relu(x)
            return tf.clip_by_value(x, 0.0, 1.0)

        def activate(x):
            logger.info("Quantizing activations to {} bits {}".format(BITA, x.name))
            return fa(nonlin(x))

        #normalize image
        #input = input/255.0

        # Define the architecture.
        # Here we are using LeNet 5 architecture (http://yann.lecun.com/exdb/lenet/), known to have high accuracy for MNIST digit classifications
        with remap_variables(quantize_weight): #quantize weights            
            # although weights are not quantized in the first layer to preserve accuracy, activation map from first layer are quantized
            # ref.section 2.7 https://arxiv.org/pdf/1606.06160.pdf
            # The layers are represented in symbolic input format:
            # ref: 
            #   https://tensorpack.readthedocs.io/tutorial/symbolic.html
            # This representation is same as tf.layers but we are using this format because the linearwrap()
            # provides apply() function that allows to apply function directly to a tensor            
            # ref. 
            #   https://tensorpack.readthedocs.io/en/latest/modules/models.html#tensorpack.models.Conv2D
            #   https://tensorpack.readthedocs.io/en/latest/_modules/tensorpack/models/linearwrap.html#LinearWrap
            
            # conv2d: The default stride is (1,1) for tf.layers so not changing those  
            logits = (LinearWrap(input)
                        .Conv2D('conv0',6, 5, padding = "same", activation= tf.nn.tanh) 
                        .AvgPooling('pool0', 2, 2, padding='valid').apply(activate) 
                        .Conv2D('conv1',16, 5, padding = "valid", activation= tf.nn.tanh)
                        .AvgPooling('pool1', 2, 2, padding='valid').apply(activate) 
                        .FullyConnected('fc0', 120, activation=tf.nn.tanh).apply(activate) 
                        .FullyConnected('fc1',84, activation=tf.nn.tanh).apply(activate) 
                        .FullyConnected('linear', 10, activation=tf.identity)())
            #can use tf.layers for quantization too but they are very slow so using the symbolic method provided by tensorpack
            """
            l = tf.layers.conv2d(input, 6, 5, padding = "same", activation= tf.nn.tanh, name='conv0')
            l = tf.layers.average_pooling2d(l, 2, 2, padding='valid')           
            l = tf.map_fn(activate, l)              
            #l = tf.vectorized_map(activate, l)                   
            l = tf.layers.conv2d(l, 16, 5, padding = "valid", activation= tf.nn.tanh, name='conv1')
            l = tf.layers.average_pooling2d(l, 2, 2, padding='valid')
            l = tf.map_fn(activate, l)     
            l = tf.layers.flatten(l)
            l = tf.layers.dense(l, 120, activation=tf.nn.tanh, name='fc0')
            l = tf.map_fn(activate, l)     
            l = tf.layers.dense(l, 84, activation=tf.nn.tanh, name='fc1')
            before_quantization = tf.identity(l, name='b_quant')
            l = tf.map_fn(activate, l)     
            after_quantization = tf.identity(l, name='a_quant')"""
               
            # get the logits layer. Logits are final layer values which are not passed to softmax function. 
            # So basically tensors computed at last layer without any activation functions
            # while crafting adv examples ART uses the logits because the softmax layer causes the output to lose its expressiveness        
            #logits = tf.layers.dense(l, 10, activation=tf.identity, name='linear')
        tf.nn.softmax(logits, name="output")
        #logits_1 = tf.identity(logits, name='quantized')
        #print(logits_1)
        tf.add_to_collection("logits", logits) 

        # a vector of length B with loss of each sample
        loss = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=label)
        loss = tf.reduce_mean(loss, name='cross_entropy_loss')  # the average cross-entropy loss

        correct = tf.cast(tf.nn.in_top_k(logits, label, 1), tf.float32, name='correct')
        accuracy = tf.reduce_mean(correct, name='accuracy')

        # This will monitor training error & accuracy (in a moving average fashion). The value will be automatically
        # 1. written to tensosrboard
        # 2. written to stat.json
        # 3. printed after each epoch
        train_error = tf.reduce_mean(1 - correct, name='train_error')
        summary.add_moving_summary(train_error, accuracy)

        # monitor histogram of all weight (of conv and fc layers) in tensorboard
        # summary.add_param_summary(('.*/kernel', ['histogram', 'rms']))
        # weight and bias variables are named layerName/kernel (eg conv0/kernel) when using tf.layers in case of 
        # tensorpack's layer definition they are named layerName/W (eg conv0/W)
        summary.add_param_summary(('.*/W', ['histogram', 'rms']))
        # the function should return the total cost to be optimized
        return loss

    #define the optimizer
    def optimizer(self):
        return tf.train.AdamOptimizer()


def get_config():
    logger.set_logger_dir(os.path.join('train_log', 'MNIST-Dorafa-{}'.format(args.dorefa)))

    # prepare dataset
    # get data from tar. The Mnist is not downloaded each time but make sure it is in folder: 
    # C:\Users\sab\Downloads\AI Testing\_Tools\DataSets\MNIST\Data
    data_train = GetMnist('train', dir= Mnist_Data_Dir) # has dictionary data_test._cache["train/test/extra"] that holds data   
    data_test = GetMnist('test', dir= Mnist_Data_Dir) 
    # No need to resize the images
    # resize image to 32x32
    # augmentors = [
    #        imgaug.Resize((32, 32)),
    #]
    # apply image augmentators
    # data_train is not an array but an object
    # data_train = AugmentImageComponent(data_train, augmentors)  
    # Separate data in batches, consider remainder as 60000/128 is 468.75 so total batched required will be 469
    
    """
    Separate data in batches, consider remainder as 60000/128 is 468.75 so total batched required will be 469
        Batched data() takes batch number and current total dataset (60000, 28, 28) and  returns an iterable list
        whose first element is the batched image data and second element is correct labels,
        so,
        list[0].shape = [128,28,28] list[1]=[128] so we see data is batched by defined stacks and then returned in batches along with
        relevant labels. This batched data iterable is then fed as input to "QueueInput" which then will queue the data in batches"
        ref:
            1. https://tensorpack.readthedocs.io/en/latest/tutorial/trainer.html#tower-trainer
            2. https://tensorpack.readthedocs.io/en/latest/tutorial/training-interface.html#with-modeldesc-and-trainconfig
    """   
    
    data_train = BatchData(data_train, 128, remainder=True) #as defined in dataflow.common.batchdata    
    data_test = BatchData(data_test, 100) #as defined in dataflow.common.batchdata
    #for i in data_train:
    #    print("batched data ", i[0].shape)
    # This does not work on windows, may be MultiProcessRunner(data_train, 5) works but not tested
    #data_train = MultiProcessRunnerZMQ(data_train, 5)
    return TrainConfig(
        data=QueueInput(data_train), 
        #as defined in input_source.inputsource.queueinput.
        #somehow during queing the las dimension on MNIST is removed, presumably because it is 1 channel (28,28,1) can be same as (28,28)
        #so for now just know that the input signature must be 28x28 and later add dimension
        callbacks=[
            ModelSaver(),   #save model after each epoch
            # Run inference(for validation) after every epoch
            # data_test is not ideal if we have validation split but now data is not split so use test data. This is a mal-practice and lazyness at best. Will correct this later
            InferenceRunner(data_test,  
                            [ScalarStats('accuracy')]) 
            #, DumpTensors(["b_quant:0", "a_quant:0"]) # for weights DumpTensors(["IdentityN:0"])
        ],
        model=Model(),
        max_epoch=1,
    ) 


def get_prediction_config(saved_model):
    """ Prediction config sets up all necessary configuration necessary for inference.

     It setups up:

           1. Input signatures   

           2. Session

           3. Tower function using the model() and input signatures 

           4. Output names 
           
     These are used later by offline predictor to setup inference graph """

    session = SmartInit(saved_model)

    predictorConfig = PredictConfig(
        model=Model(),
        session_init= session,
        input_names=['input'],
        output_names=['output'])
    return predictorConfig

def inference_mode(saved_model, image_data=None):
    #setup configurations for restoring model for predictions and for creating adversarial attacks
    predictorConfig = get_prediction_config(saved_model)
    logger.set_logger_dir(os.path.join('inference-log', 'MNIST-Dorafa-{}'.format(args.dorefa)))
    output_labels = ["0", "1", "2", "3", "4","5", "6", "7","8", "9"]

    #if the image contains adversarial image data, then get the adversarial image also
    get_adv_accuracy = False

    #get dataset to perform inference with
    if(image_data is None):
        data_test = GetMnist('test', Mnist_Data_Dir)  
        #x_test = data_test.images
        #label = data_test.labels
    else:
        #x_test = save_restore_images.load_numpy_array(image_data)
        #print(x_test.shape)
        print("Restoring adversarial images from location: {}".format(image_data))
        #data_test = save_restore_images.save_or_load_image__npz("load", image_data)

        """
            data_test here again is an iterable object with each iteration returning the image and corresponding label.
            if the loaded npz file contains adversarial image data, this will load that as well in a adversarial_image field.
            The iterable data_test in this case will then also return "adversarial_images" as a iterable component after benign_images and labels.
        """
        data_test = save_restore_images.save_or_load_adversarial_image__npz("load", image_data)
        #if the loaded image data has adversarial image component then find adversarial accuracy as well.
        get_adv_accuracy = data_test.hasadversarial

    #infer_model(predictorConfig, dataset=x_test)
    accuracy_t1, error_t1, adv_acc_t1, adv_err_t1 = infer_model(
        predictorConfig, 
        output_labels, 
        dataset=data_test, 
        get_adv_accuracy=get_adv_accuracy)    
    #print("accuracy: ", accuracy_t1)
    #print("error: ", error_t1) 

    #if(get_adv_accuracy):
    #    print("adv_accuracy: ", adv_acc_t1)
    #    print("adv_error: ", adv_err_t1)

def attack_mode(saved_model, algorithm):
    #setup configurations for restoring model for predictions and for creating adversarial attacks
    config = get_prediction_config(saved_model)

    output_labels = ["0", "1", "2", "3", "4","5", "6", "7","8", "9"]

    data_test = GetMnist('test')  
    x_test = data_test.images
    label = data_test.labels

    # Restore the saved model
    #attack_params = RestoreModel(config)

    if(algorithm.lower() == "fgsm"):
        supported_alg = SupportedAlgorithms.FSGM
    elif (algorithm.lower() == "jsma"):
        supported_alg = SupportedAlgorithms.JSMA
    elif (algorithm.lower() == "uap"):
        supported_alg = SupportedAlgorithms.UAP
    elif (algorithm.lower() == "ba"):
        supported_alg = SupportedAlgorithms.BA
    else:
        print("The {} algorithm is not supported.".format(algorithm))
    """
    # craft adversarial attacks
    adv_samples = get_adversarial_samples(config, x_test, supported_alg)
    #adv_samples = CreateFGSMAttack(attack_params, x_test) 
    #view one example data    
    save_restore_images.plot_images(adv_samples)
    #Save adversarial examples to numpy array    
    save_restore_images.save_numpy_array(adv_samples,  os.path.join("TrainedImages","mnist_conv_quantized_{}.npy".format(args.dorefa)))
    infer_model(config, adv_samples)  
    """
    #define how many images to produce
    limit = 500
    # craft adversarial attacks
    adv_samples = get_adversarial_samples(config, x_test[:limit], supported_alg)
    #adv_samples = CreateFGSMAttack(attack_params, x_test) 
    image_index = get_image_index(adv_samples)
    #Save adversarial examples to numpy array    
    saved_images = save_restore_images.save_or_load_adversarial_image__npz(
        "save", 
        os.path.join("TrainedImages", "mnist_conv_quantized_{}.npz".format(args.dorefa)),
        benign_image= x_test[:limit], 
        adversarial_image= adv_samples[:limit], 
        labels=label[:limit], 
        image_index=image_index)  
    #save first 10 images to disc
    #save_restore_images.save_image(adv_samples[:100], r"C:\Users\sab\Downloads\AI Testing\Source\Dorefanet\tensorpack\FullPrecisionModels\Results", False, labels)
    visualize_data.plot_images(adv_samples)
    #accuracy_t1, error_t1 = infer_model(config, output_labels, saved_images)
    #print("accuracy: ", accuracy_t1)
    #print("error: ", error_t1)

# Get the image index.
# Since we are creating adversarial examples from sequential images (top 10) so just get sequential array as image index
def get_image_index(data_samples):
    return(np.arange(0, data_samples.shape[0]+1, 1)) 

# Override the argument parser so that it shows error when no argument is provided.
# This is obsolete now since the "mode" is now a positional arguement (so gives error when not specified) but keeping this for future reference.
class custom_parser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)


if __name__ == '__main__':
    parser = custom_parser()
    # Provide --load as an optional argument. But this is required to specify the checkpoint when running the application on the Training mode.
    parser.add_argument('-l', '--load', 
                        help="Load previously trained model to run inference on. Example: ./checkpoint_folder/chkpt_name_without_extension")
    parser.add_argument('-a', '--algorithm', choices= ["FGSM","JSMA", "SIMBA", "UAP"],
                        help="Specify the attack algorithm. This is mandatory when running application in attack mode")
    #Argument to specify the quantization bitwidth. Default is 2,2,32
    parser.add_argument('--dorefa', required=True,
                        help = "Specify quantization bitwidth during training for (weights) W, (activation) A and (gradients) G. For example Train --dorefa \"2, 2, 32\"",
                        default='2,2,32')
    #Provide adversarial images to run the inference on (blackbox adversarial attack)
    parser.add_argument("--images",
                        help= "In inference mode, provide list of images in .npz format to use for adversarial attack on the network. When not given inference will be done on the default test data.")
  
    '''
        Provide "mode" as a positional (compulsory and position sensitive) argument.
        Mode can be either Inference, Train or Attack. Example: Python Mnist_model_fp.py Inference, this will run the app in Inference mode
        Since this is a compulsory positional arg, we just have to indicate the choice without actually mentioning mode in the syntax.
        If this was the optional argument like --mode then we have to specify choices along with the keyword like --mode "Inference"
    ''' 
    parser.add_argument( 'mode', choices= ["inference","train", "attack"],
                        help = "\"Inference\" runs the program in inference mode. --load option then is mandatory to spefify the checkpoint to load model from. \"Train\" will run the program in training mode. Training is done using MNIST dataset in a LeNet5 architecture. \"attack\" mode to create adversarial examples in this model")
    args = parser.parse_args()    
    
    if(args.mode):
        if(args.mode == "train"):
            print("...Entering training mode with quantization...")
            if(args.dorefa is None):
                print("Quantization parameters not specified, continuing with W, A, G = 2 ,2, 32")
            BITW, BITA, BITG = map(int, args.dorefa.split(','))
            config = get_config()   #get traning configuration    
            launch_train_with_config(config, SimpleTrainer()) #launch training with defined configurations
        elif(args.mode == "inference"):
            print("...Entering Inference mode...")
            if(args.load is None):
                custom_parser.error("Please provide the checkpoint via the --load option")
            else:
                if(args.images is not None):
                    print("Performing inference with adversarial examples on: {}".format(args.images))
                inference_mode(args.load, args.images)
        elif(args.mode == "attack"):
            print("...Creating adversarial examples on the model...")
            if(args.load is None):
                custom_parser.error("Please provide the checkpoint via the --load option")
            elif(args.algorithm is None):
                custom_parser.error("Please provide the algorithm to craft the adversarial example via the --algorithm option")
            else:
                attack_mode(args.load, args.algorithm)