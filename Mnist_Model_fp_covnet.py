
import argparse
import os
import sys

import tensorflow as tf
import matplotlib.pyplot as plt 
import numpy as np
from PIL import Image


import tqdm
from tensorpack.utils.stats import RatioCounter

from tensorpack import *
from tensorpack.callbacks import graph
from tensorpack.dataflow import dataset
#from tensorpack.predict.base import CreateAdvAttacks
from tensorpack.tfutils.sessinit import SmartInit
from tensorpack.tfutils.summary import add_moving_summary, add_param_summary
from enum import Enum

from tensorpack.input_source import PlaceholderInput
from tensorpack.tfutils.common import get_tensors_by_names, get_op_tensor_name
from tensorpack.tfutils.tower import PredictTowerContext
#if i call this, this will call the updated version of the file in my PC but without it tensorpack import * will call the defaule version somehow
#from tensorpack.predict.base import OfflinePredictor

from art.attacks.evasion import FastGradientMethod
from art.estimators.classification import TensorFlowClassifier

#Get custom Mnist data extraction (default tensorflow has an addition dimention that can cause problems during inference)
from DataSets.mnist import GetMnist
from Inference.infer_model import infer_model, show_one_predictions
from CreateAttacks.supported_attacks import SupportedAlgorithms
from CreateAttacks.create_adversarial_samples import get_adversarial_samples
import save_restore_images
import visualize_data

#save location for anything that needs to be saved apart from training related data
save_location = os.path.join("results")
#Mnist data location
Mnist_Data_Dir = r"C:\Users\sab\Downloads\AI Testing\_Tools\DataSets\MNIST\Data"

class Model(ModelDesc):

    '''
    Define model using the ModelDesc parent. To train or to inference a network, you would require 
    1. The dataset (to train or infer from)
    2. The model definition
    3. The input placeholder which is used to later feed data
    4. Optimizer function (required by trainer only)

    Three of these four (2,3,4) are supplied by the ModelDesc. Thus this class the core of tensorpack understanding and a
    model should be restored (especially quantized) using the ModelDesc description in order to make sure the graph is made correctly  
    
    ref: 
        1. https://tensorpack.readthedocs.io/en/latest/tutorial/inference.html#step-1-build-the-model-graph
        2. https://tensorpack.readthedocs.io/tutorial/training-interface.html#with-modeldesc-and-trainconfig
    '''


    # Provide the input signature
    # Here it should be noted that the LeNet architecture that we are impmlementing requires images to be in 32x32 format
    # By default the image size is 28x28 for MNIST, so need to resize images before feeding to the network
    def inputs(self):
                #This is TF 1.13 code so we need to specify a placeholder for the graph and feed input to it later
        return [tf.TensorSpec((None, 28, 28), tf.float32, 'input'),
                # label is a Rank 1 tensor/ an array that holds true labels for the current minibatch and hence the (?) dimension
                # While building the grpah label placeholder is used to proovide the loss which the grpah returns. 
                # So when this grpah executes, it takes input and returns loss based on comparision with "output" and "label"
                # since the mini-batch is determined later during training
                # Label tensor is used only during training and is accessable later as "Label:0" tensor
                # Label is used only during training
                tf.TensorSpec((None,), tf.int32, 'label')]

    #define the model
    def build_graph(self, input, label):
        """
            The default dataset for MNIST only has 3 dim (Batch, image_height, Image_width). In tf, one addition dimension
            for channel is required so add one additional channel at axis =3

            Here also notics that input has same dimension as in inputs() TensorSpec meaning that data or input fed is in batches
            i.e input has dim: (128, 28, 28), 128 being the batch size and thus the accuracy computed laster is of 128 data points.
            tensorflow accepts input of (BHWC); Batch, height, width and channel 
        """
        input = tf.expand_dims(input, 3)

        #normalize image
        #input = input/255.0

        # Define the architecture.
        # Here we are using LeNet 5 architecture (http://yann.lecun.com/exdb/lenet/), known to have high accuracy for MNIST digit classifications
         
        # conv2d: The default stride is (1,1) for tf.layers so not changing those
        logits = (LinearWrap(input)
                        .Conv2D('conv0',6, 5, padding = "same", activation= tf.nn.tanh) 
                        .AvgPooling('pool0', 2, 2, padding='valid')
                        .Conv2D('conv1',16, 5, padding = "valid", activation= tf.nn.tanh)
                        .AvgPooling('pool1', 2, 2, padding='valid')
                        .FullyConnected('fc0', 120, activation=tf.nn.tanh)
                        .FullyConnected('fc1',84, activation=tf.nn.tanh)
                        .FullyConnected('linear', 10, activation=tf.identity)())
        # get the logits layer. Logits are final layer values which are not passed to softmax function. 
        # So basically tensors computed at last layer without any activation functions
        # while crafting adv examples logits are used because the softmax layer causes the output to lose its expressiveness        
        #logits = tf.layers.dense(l, 10, activation=tf.identity, name='linear')
        #finally pass the logits to the softmax to get the probabilities
        tf.nn.softmax(logits, name="output")
        tf.add_to_collection("logits", logits)

        # a vector of length B with loss of each sample
        loss = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=label)
        loss = tf.reduce_mean(loss, name='cross_entropy_loss')  # the average cross-entropy loss

        correct = tf.cast(tf.nn.in_top_k(logits, label, 1), tf.float32, name='correct')
        accuracy = tf.reduce_mean(correct, name='accuracy')

        # This will monitor training error & accuracy (in a moving average fashion) This is different than the normal average. 
        # The value will be automatically
        # 1. written to tensosrboard
        # 2. written to stat.json
        # 3. printed after each epoch
        train_error = tf.reduce_mean(1 - correct, name='train_error')
        summary.add_moving_summary(train_error, accuracy)

        # monitor histogram of all weight (of conv and fc layers) in tensorboard
        # summary.add_param_summary(('.*/kernel', ['histogram', 'rms']))
        # weight and bias variables are named layerName/kernel (eg conv0/kernel) when using tf.layers in case of 
        # tensorpack's layer definition they are named layerName/W (eg conv0/W)
        summary.add_param_summary(('.*/W', ['histogram', 'rms']))
        # the function should return the total cost to be optimized
        return loss

    #define the optimizer
    def optimizer(self):
        return tf.train.AdamOptimizer()


def eval_classification(model, sessinit, dataflow):
    """
    Eval a classification model on the dataset. It assumes the model inputs are
    named "input" and "label", and contains "wrong-top1" and "wrong-top5" in the graph.
    """
    pred_config = PredictConfig(
        model=model,
        session_init=sessinit,
        input_names=['input', 'label'],
        output_names=['accuracy']
    )
    acc1 = RatioCounter()

    # This does not have a visible improvement over naive predictor,
    # but will have an improvement if image_dtype is set to float32.
    pred = FeedfreePredictor(pred_config, StagingInput(QueueInput(dataflow), device='/gpu:0'))
    for _ in tqdm.trange(dataflow.size()):
        top1 = pred()
        batch_size = top1.shape[0]
        acc1.feed(top1.sum(), batch_size)

    print("Top1 Error: {}".format(acc1.ratio))


def get_config():
    logger.set_logger_dir(os.path.join('train_log', 'MNIST-FP'))

    # prepare dataset
    # get data from tar. The Mnist is not downloaded each time but make sure it is in folder: 
    # C:\Users\sab\Downloads\AI Testing\_Tools\DataSets\MNIST\Data
    
    # GetMnist returns (data_train/ data_test) an iterable object which returns a list [image and data] in each iteration (as defined by __iter__). Apart from this we can also get
    # individual images/labels via the _cache dictionary within data_train.
    data_train = GetMnist('train', Mnist_Data_Dir) # has dictionary data_test._cache["train/test/extra"] that holds data   
    data_test = GetMnist('test', Mnist_Data_Dir) 
    # No need to resize the images
    # resize image to 32x32
    # augmentors = [
    #        imgaug.Resize((32, 32)),
    #]
    # apply image augmentators
    # data_train is not an array but an object
    # data_train = AugmentImageComponent(data_train, augmentors)  
    """
    Separate data in batches, consider remainder as 60000/128 is 468.75 so total batched required will be 469
        Batched data() takes batch number and current total dataset (60000, 28, 28) and  returns an iterable list
        whose first element is the batched image data and second element is correct labels,
        so,
        list[0].shape = [128,28,28] list[1]=[128] so we see data is batched by defined stacks and then returned in batches along with
        relevant labels. This batched data iterable is then fed as input to "QueueInput" which then will queue the data in batches"
        ref:
            1. https://tensorpack.readthedocs.io/en/latest/tutorial/trainer.html#tower-trainer
            2. https://tensorpack.readthedocs.io/en/latest/tutorial/training-interface.html#with-modeldesc-and-trainconfig
    """   
    data_train = BatchData(data_train, 128, remainder=True) #as defined in dataflow.common.batchdata 
    data_test = BatchData(data_test, 100) #as defined in dataflow.common.batchdata
    #for i in data_train:
    #    print("batched data ", i[0].shape)
    # This does not work on windows, may be MultiProcessRunner(data_train, 5) works but not tested    
    #data_train = MultiProcessRunnerZMQ(data_train, 5)
    return TrainConfig(
        data=QueueInput(data_train), 
        #as defined in input_source.inputsource.queueinput.
        #somehow during queing the las dimension on MNIST is removed, presumably because it is 1 channel (28,28,1) can be same as (28,28)
        #so for now just know that the input signature must be 28x28 and later add dimension
        callbacks=[
            ModelSaver(),   #save model after each epoch
            
                # Run inference(for validation) after every epoch
                # data_test is not ideal if we have validation split but now data is not split so use test data. This is a mal-practice and lazyness at best. Will correct this later
                # scalarStats gives the average of scalar value specified over all datapoints. This is different from ClassificationError/ or accuracy
                # Classification error also considers that batch size might be different while computing average. For instance, 
                # ClassificationError gives "true" error this is different that ScalarStats("error-rate") because here ScalarStats does not consider batch size 
                # difference and just give average over the data points.
                # However, this is true for training, for testing it seems that batch-size is 1 because inference accuracy over all batches is equal
                # to how I was computing accuracy (individual data points)
                # More at:
                    #https://tensorpack.readthedocs.io/en/latest/modules/callbacks.html#tensorpack.callbacks.ScalarStats
            
            InferenceRunner(data_test,  
                            [ScalarStats('accuracy')])
        ],
        model=Model(),
        max_epoch=1,
    ) 

def get_prediction_config(saved_model):
    """ Prediction config sets up all necessary configuration necessary for inference.

     It setups up:

           1. Input signatures   

           2. Session

           3. Tower function using the model() and input signatures 

           4. Output names 
           
     These are used later by offline predictor to setup inference graph """

    session = SmartInit(saved_model)

    predictorConfig = PredictConfig(
        model=Model(),
        session_init= session,
        input_names=['input'],
        output_names=['output'])
    return predictorConfig


def inference_mode(saved_model, image_data=None):    

    #setup configurations for restoring model for predictions and for creating adversarial attacks
    predictorConfig = get_prediction_config(saved_model)
    logger.set_logger_dir(os.path.join('inference-log', 'MNIST-FP'))
    #data labels, one of these will have high prob than others during inference
    output_labels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    
    #if the image contains adversarial image data, then get the adversarial image also
    get_adv_accuracy = False

    if(image_data is None):
        #get dataset to perform inference with
        data_test = GetMnist('test', Mnist_Data_Dir)  
        #x_test = data_test.images
        #labels = data_test.labels
    else:
        print("Restoring images from location: {}".format(image_data))
                
        #data_test = save_restore_images.save_or_load_image__npz("load", image_data)
        """
            data_test here again is an iterable object with each iteration returning the image and corresponding label.
            if the loaded npz file contains adversarial image data, this will load that as well in a adversarial_image field.
            The iterable data_test in this case will then also return "adversarial_images" as a iterable component after benign_images and labels.
        """
        data_test = save_restore_images.save_or_load_adversarial_image__npz("load", image_data)
        #if the loaded image data has adversarial image component then find adversarial accuracy as well.
        get_adv_accuracy = data_test.hasadversarial

    #eval_classification(Model(), session, data_test)

    #im = Image.fromarray(x_test[2])
    #im.save(r'C:\tmp\SaveImages\{}.png'.format(y_test[2]))
    #predictor = OfflinePredictor(predictorConfig)
    #show_one_predictions(x_test[9922], labels[9922], predictor, output_labels)
    #infer_model(predictorConfig, dataset=x_test)
    accuracy_t1, error_t1, adv_acc_t1, adv_err_t1 = infer_model(
        predictorConfig, 
        output_labels, 
        dataset=data_test, 
        get_adv_accuracy=get_adv_accuracy)  
    #print("accuracy: ", accuracy_t1)
    #print("error: ", error_t1)     
    #if(get_adv_accuracy):
    #    print("adv_accuracy: ", adv_acc_t1)
    #    print("adv_error: ", adv_err_t1)

def attack_mode(saved_model, algorithm, image_data=None):
    #setup configurations for restoring model for predictions and for creating adversarial attacks
    config = get_prediction_config(saved_model)
   
    #data labels, one of these will have high prob than others during inference
    output_labels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

    # when dataset is None this means that adversarial samples have to be trained with the current model. 
    # This requires algorithm selection from user    
    if(algorithm is not None):        
        if(algorithm.lower() == "fgsm"):
            supported_alg = SupportedAlgorithms.FSGM
        elif (algorithm.lower() == "jsma"):
            supported_alg = SupportedAlgorithms.JSMA
        elif (algorithm.lower() == "uap"):
            supported_alg = SupportedAlgorithms.UAP
        elif (algorithm.lower() == "ba"):
            supported_alg = SupportedAlgorithms.BA
        else:
            print("The {} algorithm is not supported.".format(algorithm))        
        
        #get dataset to perform inference with
        data_test = GetMnist('test', Mnist_Data_Dir)  
        x_test = data_test.images
        labels = data_test.labels

        #CREATE ADVERARIAL IMAGES HERE
        #define how many images to produce
        limit = 500
        # craft adversarial attacks
        adv_samples = get_adversarial_samples(config, x_test[:limit], supported_alg)
        #adv_samples = CreateFGSMAttack(attack_params, x_test) 
        #Save adversarial examples to numpy array    
        #saved_images = save_restore_images.save_or_load_image__npz("save", os.path.join("TrainedImages", "mnist_conv_full_pre.npz"), adv_samples[:limit], labels[:limit])
        
        image_index = get_image_index(adv_samples)
        saved_images = save_restore_images.save_or_load_adversarial_image__npz(
            "save", 
            os.path.join("TrainedImages", "MNIST-FP\mnist_conv_full_adv_pre.npz"), 
            benign_image= x_test[:limit], 
            adversarial_image= adv_samples[:limit], 
            labels=labels[:limit], 
            image_index=image_index)

        #save first 10 images to disc
        #save_restore_images.save_image(adv_samples[:100], r"C:\Users\sab\Downloads\AI Testing\Source\Dorefanet\tensorpack\FullPrecisionModels\Results", False, labels)
        visualize_data.plot_images(adv_samples)
        #infer_model(config, adv_samples)
        print("Adversarial images were crafter using {} algorithm and were saved in location {}".format(algorithm, os.path.join("TrainedImages", "mnist_conv_full_adv_pre.npz")))
        #accuracy_t1, error_t1 = infer_model(config, output_labels, saved_images, purpose="attack")
        #print("accuracy: ", accuracy_t1)
        #print("error: ", error_t1)

    #Othewise load adversarial image from the selected loacation and attack the model
    elif(image_data is not None):
        print("Restoring adversarial images from location: {}".format(image_data))        
        #data_test here again is an iterable object with each iteration returning adversarial_image, its benignCounterpart and label
        data_test = save_restore_images.save_or_load_adversarial_image__npz("load", image_data)
        accuracy_t1, error_t1 = infer_model(config, output_labels, data_test, purpose="attack")
    else:
        print("Please provide algorithm to craft adversarial examples in this network.",
                "For this use --algorithm option. See help to view supported algorithms.",
                "Alternatively, use --image to load currently existing images to create an adversarial attack on the model with saved .npz format image data")
    
 
# Get the image index.
# Since we are creating adversarial examples from sequential images (top 10) so just get sequential array as image index
def get_image_index(data_samples):
    return(np.arange(1, data_samples.shape[0]+1, 1))  

# Override the argument parser so that it shows error when no argument is provided.
# This is obsolete now since the "mode" is now a positional arguement (so gives error when not specified) but keeping this for future reference.
class custom_parser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)


if __name__ == '__main__':
    
    parser = custom_parser()
    # Provide --load as an optional argument. But this is required to specify the checkpoint when running the program in the Training mode.
    parser.add_argument('-l', '--load', 
                        help="Load previously trained model to run inference on. Example: ./checkpoint_folder/chkpt_name_without_extension")
    parser.add_argument('-a', '--algorithm', choices= ["FGSM","JSMA", "SIMBA", "UAP"],
                        help="Specify the attack algorithm. This is mandatory when running application in attack mode")
    '''
        Provide "mode" as a positional (compulsory and position sensitive) argument.
        Mode can be either Inference or Train. Example: Python Mnist_model_fp.py Inference ,  this will run the app in Inference mode
        Since this is a compulsory positional arg, we just have to indicate the choice without actually mentioning mode in the syntax.
        If this was the optional argument like --mode then we have to specify choices along with the keyword like --mode "Inference"
    ''' 
    parser.add_argument( 'mode', choices= ["inference","train", "attack"],
                        help = "\"Inference\" runs the program in inference mode. --load option then is mandatory to spefify the checkpoint to load model from. \"Train\" will run the program in training mode. Training is done using MNIST dataset in a LeNet5 architecture. \"attack\" mode to create adversarial examples in this model")
    parser.add_argument("--images",
                        help= "In inference mode, provide list of images in .npz format to use for adversarial attack on the network. When not given inference will be done on the default test data.")
    args = parser.parse_args()    
    
    if(args.mode):
        if(args.mode == "train"):
            print("Entering training mode...")
            config = get_config()   #get traning configuration    
            launch_train_with_config(config, SimpleTrainer()) #launch training with defined configurations
        elif(args.mode == "inference"):
            print("Entering Inference mode...")
            if(args.load is None):
                print("Please provide the checkpoint via the --load option")
            else:
                if(args.images is not None):
                    print("Performing inference with adversarial examples on: {}".format(args.images))
                inference_mode(args.load, args.images)
        elif(args.mode == "attack"):
            print("...Creating adversarial examples on the model...")
            if(args.load is None):
                # ART will create adeversarial examples out of an untrained model if you just give it a model. 
                # Probably the weights and biases are randomly generated. It is important thus to load model checkpoint before 
                # If you dont load model, it will still work!! but it is not correct to do so.
                parser.error("Please provide the checkpoint via the --load option")
            else:
                if(args.algorithm is not None):                
                    print("Creating adversarial examples using selected algorithm...")
                else:
                     parser.error("Please provide either algorithm to craft adversarial examples in this model via the --algorithm option.")
                #elif(args.images is not None):
                #    print("Performing attacks on the model using images loaded from {}", args.images)
                #else:
                #    parser.error("Please provide either algorithm to craft adversarial examples in this model via the --algorithm option. Alternatively, provide .npz file with adversarial images to attack the model.")
                attack_mode(args.load, args.algorithm, args.images)