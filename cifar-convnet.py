#!/usr/bin/env python
# File: cifar-convnet.py
# Author: Yuxin Wu
import argparse
import os
import sys
import matplotlib.pyplot as plt
import numpy as np
from tensorpack.compat import tfv1 as tf

from tensorpack import *
from tensorpack.dataflow import dataset
from tensorpack.tfutils.summary import *
from tensorpack.utils.gpu import get_num_gpu



"""
A small convnet model for Cifar10 or Cifar100 dataset.

Cifar10 trained on 1 GPU:
    91% accuracy after 50k iterations.
    79 itr/s on P100

Not a good model for Cifar100, just for demonstration.
"""


class Model(ModelDesc):
    def __init__(self, cifar_classnum):
        super(Model, self).__init__()
        self.cifar_classnum = cifar_classnum

    def inputs(self):
        return [tf.TensorSpec((None, 30, 30, 3), tf.float32, 'input'),
                tf.TensorSpec((None,), tf.int32, 'label')]

    def build_graph(self, image, label):
        drop_rate = tf.constant(0.5 if self.training else 0.0)

        if self.training:
            tf.summary.image("train_image", image, 10)
        if tf.test.is_gpu_available():
            image = tf.transpose(image, [0, 3, 1, 2])
            data_format = 'channels_first'
        else:
            data_format = 'channels_last'

        image = image / 4.0     # just to make range smaller
        with argscope(Conv2D, activation=BNReLU, use_bias=False, kernel_size=3), \
                argscope([Conv2D, MaxPooling, BatchNorm], data_format=data_format):
            logits = LinearWrap(image) \
                .Conv2D('conv1.1', filters=64) \
                .Conv2D('conv1.2', filters=64) \
                .MaxPooling('pool1', 3, stride=2, padding='SAME') \
                .Conv2D('conv2.1', filters=128) \
                .Conv2D('conv2.2', filters=128) \
                .MaxPooling('pool2', 3, stride=2, padding='SAME') \
                .Conv2D('conv3.1', filters=128, padding='VALID') \
                .Conv2D('conv3.2', filters=128, padding='VALID') \
                .FullyConnected('fc0', 1024 + 512, activation=tf.nn.relu) \
                .Dropout(rate=drop_rate) \
                .FullyConnected('fc1', 512, activation=tf.nn.relu) \
                .FullyConnected('linear', out_dim=self.cifar_classnum)()

        cost = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=label)
        cost = tf.reduce_mean(cost, name='cross_entropy_loss')

        correct = tf.cast(tf.nn.in_top_k(predictions=logits, targets=label, k=1), tf.float32, name='correct')
        # monitor training error
        add_moving_summary(tf.reduce_mean(correct, name='accuracy'))

        # weight decay on all W of fc layers
        wd_cost = regularize_cost('fc.*/W', l2_regularizer(4e-4), name='regularize_loss')
        add_moving_summary(cost, wd_cost)

        add_param_summary(('.*/W', ['histogram']))   # monitor W
        return tf.add_n([cost, wd_cost], name='cost')

    def optimizer(self):
        lr = tf.get_variable('learning_rate', initializer=1e-2, trainable=False)
        tf.summary.scalar('lr', lr)
        return tf.train.AdamOptimizer(lr, epsilon=1e-3)


def get_data(train_or_test, cifar_classnum):
    isTrain = train_or_test == 'train'
    if cifar_classnum == 10:
        ds = dataset.Cifar10(train_or_test)
    else:
        ds = dataset.Cifar100(train_or_test)
    if isTrain:
        augmentors = [
            imgaug.RandomCrop((30, 30)),
            imgaug.Flip(horiz=True),
            imgaug.Brightness(63),
            imgaug.Contrast((0.2, 1.8)),
            imgaug.MeanVarianceNormalize(all_channel=True)
        ]
    else:
        augmentors = [
            imgaug.CenterCrop((30, 30)),
            imgaug.MeanVarianceNormalize(all_channel=True)
        ]
    ds = AugmentImageComponent(ds, augmentors)
    ds = BatchData(ds, 128, remainder=not isTrain)
    #if isTrain: # no support for windows
       # ds = MultiProcessRunnerZMQ(ds, 5) # check MultiProcessRunner for windows
    return ds


def get_config(cifar_classnum):
    # prepare dataset
    dataset_train = get_data('train', cifar_classnum)
    dataset_test = get_data('test', cifar_classnum)

    def lr_func(lr):
        if lr < 3e-5:
            raise StopTraining()
        return lr * 0.31
    return TrainConfig(
        model=Model(cifar_classnum),
        data=QueueInput(dataset_train),
        callbacks=[
            ModelSaver(),
            InferenceRunner(dataset_test,
                            ScalarStats(['accuracy', 'cost'])),
            StatMonitorParamSetter('learning_rate', 'validation_accuracy', lr_func,
                                   threshold=0.001, last_k=10, reverse=True),
        ],
        max_epoch=150,
    )

#added
def run_image(model, sess_init, inputs):
    cif=dataset.Cifar10('test')
    classes= cif.get_label_names()
    pred_config = PredictConfig(
        model=model,
        session_init=sess_init,
        input_names=['input'],
        output_names=['linear/output'] #if name not found, do:
    )
    """insert code:
        all_ops = tf.get_default_graph().get_operations()
        print('all ops:',all_ops)
        
        into file: Path to your anaconda environment+ \lib\site-packages\tensorpack\predict\base.py
        e.g. after line input_tensors = get_tensors_by_names(config.input_names)
        and find the name of the layer which looks like model output"""
    predictor = OfflinePredictor(pred_config)
    words = [0,1,2,3,4,5,6,7,8,9]
    #print('inputs:',inputs.shape)

    for f in inputs:
        print('f=',f.shape)
        #print('shape of image:' ,f.shape)
        plt.imshow(f)
        #Actually displaying the plot if you are not in interactive mode
        
        plt.show()
        augmentors = [
            imgaug.CenterCrop((30, 30)),
            imgaug.MeanVarianceNormalize(all_channel=True)
        ]
        transformers = imgaug.AugmentorList(augmentors)
        img = transformers.augment(f)[np.newaxis, :, :, :] # original shape is (32,32,3). Network takes (30,30,3) -> crop the image
        outputs = predictor(img) [0] 
        prob = outputs[0]
        ret = prob.argsort()[-10:][::-1]
        names = [words[i] for i in ret]
        names_text=[ classes[i] for i in ret]
        #print(f + ":")
        print(list(zip(names,names_text, prob[ret])))
#fin


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', help='comma separated list of GPU(s) to use.')
    parser.add_argument('--load', help='load model')
    parser.add_argument('--classnum', help='10 for cifar10 or 100 for cifar100',
                        type=int, default=10)
    parser.add_argument('--inference', help='enter something to run inference on a saved model',
                        type=int, default=10)
    args = parser.parse_args()

    if args.gpu:
        os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    #added
    
    #to run inference on a saved model, enter:  
    #python  .\cifar-convnet.py --load *Path to your train_log*\train_log\cifar10\checkpoint --inference 1

    if args.inference==1: #run inference
        test_images_with_labels =dataset.Cifar10('test').data[0:5] # take first x images from test set. Caution: cifar10 or 100?
        cif=dataset.Cifar10('test')
        classes= cif.get_label_names()
        [print(test_images_with_labels[i][1],' ;',classes[test_images_with_labels[i][1]]) for i in range(len(test_images_with_labels))]
       
        test_images=[test_images_with_labels[i][0] for i in range(len(test_images_with_labels))] # dataset.Cifar10 returns a list [[image1, label1], [image2, label2], ...] 
        model=Model(10)
        run_image(model, SmartInit(args.load),test_images)
        sys.exit()
    #fin

    with tf.Graph().as_default():
        logger.set_logger_dir(os.path.join('train_log', 'cifar' + str(args.classnum)))
        config = get_config(args.classnum)
        config.session_init = SmartInit(args.load)

        num_gpu = get_num_gpu()
        trainer = SimpleTrainer() if num_gpu <= 1 \
            else SyncMultiGPUTrainerParameterServer(num_gpu)
        launch_train_with_config(config, trainer)
