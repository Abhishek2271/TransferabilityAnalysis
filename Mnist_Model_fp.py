
import argparse
import os
import sys

import tensorflow as tf
import matplotlib.pyplot as plt 
import numpy as np

from tensorpack import *
from tensorpack.callbacks import graph
from tensorpack.dataflow import dataset
#from tensorpack.predict.base import CreateAdvAttacks
from tensorpack.tfutils.sessinit import SmartInit
from tensorpack.tfutils.summary import add_moving_summary, add_param_summary
from enum import Enum

from tensorpack.input_source import PlaceholderInput
from tensorpack.tfutils.common import get_tensors_by_names, get_op_tensor_name
from tensorpack.tfutils.tower import PredictTowerContext
#if i call this, this will call the updated version of the file in my PC but without it tensorpack import * will call the defaule version somehow
#from tensorpack.predict.base import OfflinePredictor

from art.attacks.evasion import FastGradientMethod
from art.estimators.classification import TensorFlowClassifier

#Get custom Mnist data extraction (default tensorflow has an addition dimention that can cause problems during inference)
from DataSets.mnist import GetMnist
from Inference.infer_model import infer_model
from CreateAttacks.supported_attacks import SupportedAlgorithms
from CreateAttacks.create_adversarial_samples import get_adversarial_samples

Mnist_Data_Dir = r"C:\Users\sab\Downloads\AI Testing\_Tools\DataSets\MNIST\Data"

'''
    Define model using the ModelDesc parent. To train or to inference a network, you would require 
    1. The dataset (to train or infer from)
    2. The model definition
    3. The input placeholder which is used to later feed data
    4. Optimizer function (required by trainer only)

    Three of these four (2,3,4) are supplied by the ModelDesc. Thus this class the core of tensorpack understanding and a
    model should be restored (especially quantized) using the ModelDesc description in order to make sure the graph is made correctly
    ref: 
        1. https://tensorpack.readthedocs.io/en/latest/tutorial/inference.html#step-1-build-the-model-graph
        2. https://tensorpack.readthedocs.io/tutorial/training-interface.html#with-modeldesc-and-trainconfig
'''
class Model(ModelDesc):

    # Provide the input signature
    # Here it should be noted that the LeNet architecture that we are impmlementing requires images to be in 32x32 format
    # By default the image size is 28x28 for MNIST, so need to resize images before feeding to the network
    def inputs(self):
        return [tf.TensorSpec((None, 28, 28), tf.float32, 'input'),
                tf.TensorSpec((None,), tf.int32, 'label')]

    #define the model
    def build_graph(self, input, label):
        """
            The default dataset for MNIST only has 3 dim (Batch, image_height, Image_width). In tf, one addition dimension
            for channel is required so add one additional channel at axis =3
        """
        input = tf.expand_dims(input, 3)
        print("input shape is,", input.shape)
        print("label shape is,", label.shape)
        #normalize image
        #input = input/255.0

        # Define the architecture.
        # Here we are using LeNet 5 architecture (http://yann.lecun.com/exdb/lenet/), known to have high accuracy for MNIST digit classifications
         
        # conv2d: The default stride is (1,1) for tf.layers so not changing those
        l = tf.layers.conv2d(input, 6, 5, padding = "same", activation= tf.nn.tanh, name='conv0')
        l = tf.layers.average_pooling2d(l, 2, 2, padding='valid')
        l = tf.layers.conv2d(l, 16, 5, padding = "valid", activation= tf.nn.tanh, name='conv1')
        l = tf.layers.average_pooling2d(l, 2, 2, padding='valid')
        l = tf.layers.flatten(l)
        l = tf.layers.dense(l, 120, activation=tf.nn.tanh, name='fc0')
        l = tf.layers.dense(l, 84, activation=tf.nn.tanh, name='fc1')
        # get the logits layer. Logits are final layer values which are not passed to softmax function. 
        # So basically tensors computed at last layer without any activation functions
        # while crafting adv examples logits are used because the softmax layer causes the output to lose its expressiveness        
        logits = tf.layers.dense(l, 10, activation=tf.identity, name='linear')
        tf.nn.softmax(logits, name="output")
        tf.add_to_collection("logits", logits)

        print("logits shape is:", logits.shape)
        # a vector of length B with loss of each sample
        loss = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=label)
        loss = tf.reduce_mean(loss, name='cross_entropy_loss')  # the average cross-entropy loss

        correct = tf.cast(tf.nn.in_top_k(logits, label, 1), tf.float32, name='correct')
        accuracy = tf.reduce_mean(correct, name='accuracy')

        # This will monitor training error & accuracy (in a moving average fashion). The value will be automatically
        # 1. written to tensosrboard
        # 2. written to stat.json
        # 3. printed after each epoch
        train_error = tf.reduce_mean(1 - correct, name='train_error')
        summary.add_moving_summary(train_error, accuracy)

        # monitor histogram of all weight (of conv and fc layers) in tensorboard
        # summary.add_param_summary(('.*/kernel', ['histogram', 'rms']))
        # weight and bias variables are named layerName/kernel (eg conv0/kernel) when using tf.layers in case of 
        # tensorpack's layer definition they are named layerName/W (eg conv0/W)
        summary.add_param_summary(('.*/kernel', ['histogram', 'rms']))
        # the function should return the total cost to be optimized
        return loss

    #define the optimizer
    def optimizer(self):
        return tf.train.AdamOptimizer()


def get_config():
    logger.set_logger_dir(os.path.join('train_log', 'MNIST-FP'))

    # prepare dataset
    # get data from tar. The Mnist is not downloaded each time but make sure it is in folder: 
    # C:\Users\sab\Downloads\AI Testing\_Tools\DataSets\MNIST\Data
    data_train = GetMnist('train', Mnist_Data_Dir) # has dictionary data_test._cache["train/test/extra"] that holds data   
    data_test = GetMnist('test', Mnist_Data_Dir) 
    # No need to resize the images
    # resize image to 32x32
    # augmentors = [
    #        imgaug.Resize((32, 32)),
    #]
    # apply image augmentators
    # data_train is not an array but an object
    # data_train = AugmentImageComponent(data_train, augmentors)  
    """Separate data in batches, consider remainder as 60000/128 is 468.75 so total batched required will be 469
        batched data returns an iterable list whose first element is the batched image data and second element is correct labels,so
        list[0].shape = [128,28,28] list[1]=[128] so we see data is batched by defined stacks and then returned in batches along with
        relevant labels. This batched data iterable is then fed as input to "QueueInput" which then will queue the data in batches"
    """
    data_train = BatchData(data_train, 128, remainder=True) #as defined in dataflow.common.batchdata    
    
    #for i in data_train:
    #    print("batched data ", i[0].shape)
    data_test = BatchData(data_test, 100) #as defined in dataflow.common.batchdata
    # This does not work on windows, may be MultiProcessRunner(data_train, 5) works but not tested
    #data_train = MultiProcessRunnerZMQ(data_train, 5)
    return TrainConfig(
        data=QueueInput(data_train), 
        #as defined in input_source.inputsource.queueinput.
        #somehow during queing the las dimension on MNIST is removed, presumably because it is 1 channel (28,28,1) can be same as (28,28)
        #so for now just know that the input signature must be 28x28 and later add dimension
        callbacks=[
            ModelSaver(),   #save model after each epoch
            # Run inference(for validation) after every epoch
            # data_test is not ideal if we have validation split but now data is not split so use test data. This is a mal-practice and lazyness at best. Will correct this later
            InferenceRunner(data_test,  
                            [ScalarStats('cross_entropy_loss')])
        ],
        model=Model(),
        max_epoch=10,
    ) 

def inference_mode(saved_model):
    session = SmartInit(saved_model)
    predictorConfig = PredictConfig(
        model=Model(),
        session_init= session,
        input_names=['input'],
        output_names=['output'])

    #get dataset to perform inference with
    data_test = GetMnist('test', Mnist_Data_Dir)  
    x_test = data_test.images
    y_test = data_test.labels


    infer_model(predictorConfig, dataset=x_test)

def attack_mode(saved_model, algorithm):
    #setup configurations for restoring model for predictions and for creating adversarial attacks
    session = SmartInit(saved_model)
    config = PredictConfig(
        model=Model(),
        session_init= session,
        input_names=['input'],
        output_names=['output'])

    data_test = GetMnist('test')  
    x_test = data_test.images
    y_test = data_test.labels

    # Restore the saved model
    #attack_params = RestoreModel(config)

    if(algorithm == "FGSM"):
        supported_alg = SupportedAlgorithms.FSGM
    elif (algorithm == "JSMA"):
        supported_alg = SupportedAlgorithms.JSMA
    elif (algorithm == "UAP"):
        supported_alg = SupportedAlgorithms.UAP
    elif (algorithm == "SIMBA"):
        supported_alg = SupportedAlgorithms.SimBA

    # craft adversarial attacks
    adv_samples = get_adversarial_samples(config, x_test, supported_alg)
    #adv_samples = CreateFGSMAttack(attack_params, x_test) 
    #view one example data    
    plot_image(adv_samples)

    infer_model(config, adv_samples)    

def plot_image(data):
    fig = plt.figure(figsize=(15, 7))
    plt.title("Adversial examples")
    plt.imshow(data[0],cmap=plt.cm.gray_r, interpolation='nearest')
    plt.show()

# Override the argument parser so that it shows error when no argument is provided.
# This is obsolete now since the "mode" is now a positional arguement (so gives error when not specified) but keeping this for future reference.
class custom_parser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)


if __name__ == '__main__':
    parser = custom_parser()
    # Provide --load as an optional argument. But this is required to specify the checkpoint when running the program in the Training mode.
    parser.add_argument('-l', '--load', 
                        help="Load previously trained model to run inference on. Example: ./checkpoint_folder/chkpt_name_without_extension")
    parser.add_argument('-a', '--algorithm', choices= ["FGSM","JSMA", "SIMBA", "UAP"],
                        help="Specify the attack algorithm. This is mandatory when running application in attack mode")
    '''
        Provide "mode" as a positional (compulsory and position sensitive) argument.
        Mode can be either Inference or Train. Example: Python Mnist_model_fp.py Inference ,  this will run the app in Inference mode
        Since this is a compulsory positional arg, we just have to indicate the choice without actually mentioning mode in the syntax.
        If this was the optional argument like --mode then we have to specify choices along with the keyword like --mode "Inference"
    ''' 
    parser.add_argument( 'mode', choices= ["inference","train", "attack"],
                        help = "\"Inference\" runs the program in inference mode. --load option then is mandatory to spefify the checkpoint to load model from. \"Train\" will run the program in training mode. Training is done using MNIST dataset in a LeNet5 architecture. \"attack\" mode to create adversarial examples in this model")
    args = parser.parse_args()    
    
    if(args.mode):
        if(args.mode == "train"):
            print("Entering training mode...")
            config = get_config()   #get traning configuration    
            launch_train_with_config(config, SimpleTrainer()) #launch training with defined configurations
        elif(args.mode == "inference"):
            print("Entering Inference mode...")
            if(args.load is None):
                print("Please provide the checkpoint via the --load option")
            else:
                inference_mode(args.load)
        elif(args.mode == "attack"):
            print("Creating adversarial attacks on the model...")
            if(args.load is None):
                print("Please provide the checkpoint via the --load option")
            if(args.algorithm is None):
                print("Please provide the algorithm to craft the adversarial example via the --algorithm option")
            else:
                attack_mode(args.load, args.algorithm)