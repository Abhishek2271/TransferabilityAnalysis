from enum import Enum


#List all supported algorithms
class SupportedAlgorithms(Enum):
    FSGM = "fgsm"
    DLV = "dlv" 
    UAP = "uap"
    JSMA = "jsma" 
    BA = "ba"



